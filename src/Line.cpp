#include "Line.hpp"
Line::Line() {

}

void Line::setup() {
	//create a polyline with an origin
	//lines have a lifespan
	//lines have a direction
	//lines have steps
	//step sizes are fed into the step function
	//lines can be glitched
	glm::vec3 start = { ofGetMouseX() , ofGetMouseY(), 0 };
	line;

	line.addVertex(start);
	stepMin = 75;
	stepMax = 125;//INCREASE LATER
	stepAxis;
	stepDir;
	stepSize = ofRandom(stepMin, stepMax);

	lifespan = 255;
	decayRate = ofRandom(5, 18);

	mainAlpha = lifespan;
	secondaryAlpha = lifespan - 45;
	glitchAlpha = lifespan +50;
	mainC;
	secondaryC;
	//NOTE: ADD IN MULTIPLE COLORWAYS
	//int colorScheme = ofRandom(-4, 9);

	//if (colorScheme > 2) {
	//	//mainC = ofColor(37, 128, 255, mainAlpha);
	//	//mainC = ofColor(6, 111, 255, mainAlpha);
	//	//secondaryC = ofColor(255, 0, 200, secondaryAlpha);
	//	//secondaryC = ofColor(254, 1, 154, secondaryAlpha);
	//	mainC = ofColor(0, 0, 255, mainAlpha);
	//	secondaryC = ofColor(255, 0, 255, secondaryAlpha);
	//}
	//else if (colorScheme > -1) {
	//	//temporarily the same color scheme, change later
	//	//mainC = ofColor(37, 128, 255, mainAlpha);
	//	mainC = ofColor(255, 0, 0, mainAlpha);
	//	//secondaryC = ofColor(255, 0, 200, secondaryAlpha);
	//	secondaryC = ofColor(255, 255, 0, secondaryAlpha);
	//}
	//else {
	//	//temporarily the same color scheme, change later
	//	//mainC = ofColor(37, 128, 255, mainAlpha);
	//	mainC = ofColor(0, 255, 0, mainAlpha);
	//	//secondaryC = ofColor(255, 0, 200, secondaryAlpha);
	//	secondaryC = ofColor(0, 255, 255, secondaryAlpha);
	//}
	glitchC = ofColor(255, 255, 255, glitchAlpha);
	glitched = false;
}

void Line::setColor(int colorScheme) {
	if (colorScheme > 2) {
		//mainC = ofColor(37, 128, 255, mainAlpha);
		//mainC = ofColor(6, 111, 255, mainAlpha);
		//secondaryC = ofColor(255, 0, 200, secondaryAlpha);
		//secondaryC = ofColor(254, 1, 154, secondaryAlpha);
		mainC = ofColor(0, 0, 255, mainAlpha);
		secondaryC = ofColor(255, 0, 255, secondaryAlpha);
	}
	else if (colorScheme > -1) {
		//temporarily the same color scheme, change later
		//mainC = ofColor(37, 128, 255, mainAlpha);
		mainC = ofColor(255, 0, 0, mainAlpha);
		//secondaryC = ofColor(255, 0, 200, secondaryAlpha);
		secondaryC = ofColor(255, 255, 0, secondaryAlpha);
	}
	else {
		//temporarily the same color scheme, change later
		//mainC = ofColor(37, 128, 255, mainAlpha);
		mainC = ofColor(0, 255, 0, mainAlpha);
		//secondaryC = ofColor(255, 0, 200, secondaryAlpha);
		secondaryC = ofColor(0, 255, 255, secondaryAlpha);
	}
}
void Line::update() {
	//update the line with a new vertex
	stepSize = ofRandom(stepMin, stepMax);

	//adds a new vertex
	step(stepSize);

	//cout << line[line.size() - 1] << endl;
	//decays the colors
	lifespan = MAX(0, lifespan - decayRate);
	decay(lifespan);
}

void Line::step(int stepSize) {
	int prevStepAxis = stepAxis; //IF WE DO THE DIRECTION CHECK
	int prevDir = stepDir; //IF WE DO THE DIRECTION CHECK

	//generates new vars
	stepAxis = glm::round(ofRandom(0, 1));
	//stepAxis = ofRandom(-2, 2);//ofRandomf();
	stepDir = glm::round(ofRandom(0, 1));
	if (stepDir == 0) {
		stepDir = -1;
	}
	//cout << stepDir << endl;
	glm::vec3 stepV;
	//cout << "stepAxis" + std::to_string(stepAxis) << endl;

	//ensures that we don't ever go backwards on the same line
	//NOTE: IS THIS NECESSARY? IF STEP SIZE IS DIFFERENT, THIS JUST LEADS TO SPIKIER LONGER LINES
	//MIGHT BE COOL TO CHECK THIS OUT
	if (prevStepAxis == stepAxis && prevDir != stepDir) {
		stepDir *= -1;
	}

	//moves on X-axis
	if (stepAxis == 0) {
		stepV = { stepDir*stepSize, 0, 0 };
	}
	//moves on Y-axis
	else {
		stepV = { 0, stepDir*stepSize,0 };
	}

	//updates line with new vertex
	int lastIndex = line.size() - 1;
	glm::vec3 newPoint = line[lastIndex] + stepV;
	//cout << newPoint << endl;
	//newPoint.x = ofClamp(newPoint.x, 0, ofGetWindowWidth());
	line.addVertex(newPoint);
}

void Line::decay(int lifespan) {
	//int newLife = lifespan - decayRate;
	//lifespan -= decayRate;
	//cout << lifespan << endl;
	mainAlpha = lifespan;
	secondaryAlpha = MAX(0, lifespan - 45);
	glitchAlpha = MAX(0, lifespan - 10);
	mainC = ofColor(mainC, mainAlpha);
	secondaryC = ofColor(secondaryC, secondaryAlpha);
	glitchC = ofColor(glitchC, glitchAlpha);
	//cout << secondaryC << endl;
}

ofPolyline Line::glitch2() {
	ofPolyline glitch2Line = line;
	glitched = false;
	int base = ofRandom(0, ofGetWindowWidth() - 300);
	for (int i = 0; i < glitch2Line.size(); i++) {
		if (glitch2Line[i].x > base && glitch2Line[i].x < base+25) {
			glitch2Line[i] += {125, 0, 0};
			glitched = true;
		}
	}
	return glitch2Line;
}
/*
ofPolyline Line::glitch() {
	ofPolyline glitchedLine = line;
	glm::vec3 glitchV;
	if (line.size() < 3) {
		glitched = false;
		return line;
	}
	//we can sub in another random for stepSize
	//checks all points for matched direction stuff
	int dirGlitch = std::round((int)ofRandom(0, 2));
	if (dirGlitch == 0) {
		dirGlitch = -1;
	}
	for (int i = 3; i < line.size() - 3; i++) {
		//if a continuous line on the x-axis, glitch middle point randomly up or down
		if (line[i - 1].x == line[i].x && line[i].x == line[i + 1].x) {
			glitchV = { dirGlitch*stepSize/3,0 ,0};
			glitchedLine[i] += glitchV;
			 glitched = true;
		}
		else if (line[i - 1].y == line[i].y && line[i].y == line[i + 1].y) {
			cout << "MADE IT" << endl;

			glitchV = { 0, dirGlitch*stepSize/3,0 };
			cout<<glitchedLine[i] << endl;
			glitchedLine[i] += glitchV;
			cout << glitchedLine[i] << endl;
			 glitched = true;
		}
	}
	cout << glitched << endl;
	return glitchedLine;
}

void Line::draw() {
	ofPushMatrix();
	ofTranslate(10, 6);
	ofPolyline glitchedLine = glitch();
	int chance = ofRandom(0, 20);
	if (glitched && chance < 5) {
		ofSetColor(secondaryC);
		line.draw();

		for (int i = 0; i < glitchedLine.size(); i++) {


			if (glitchedLine[i] != line[i]) {
				ofPolyline temp;
				glm::vec3 diff = line[i] - glitchedLine[i];
				if (diff.x != 0) {
					temp.addVertex(line[i - 1].x, glitchedLine[i].y -4,0);
					temp.addVertex(glitchedLine[i]);
					temp.addVertex(line[i + 1].x, glitchedLine[i].y + 4,0);

				}
				else {
					temp.addVertex(glitchedLine[i].x - 4, line[i - 1].y, 0);
					temp.addVertex(glitchedLine[i]);
					temp.addVertex(glitchedLine[i].x + 4, line[i + 1].y, 0);
					if ((int)ofRandom(0, 20) < 5) {
						ofSetColor(glitchC);
						temp.draw();

					}
				}
				//ofTranslate(3, 3, 0);
			}

		}



		ofPopMatrix();
		ofSetColor(mainC);
		line.draw();
	}
	else {
		//cout << "not glitched" << endl;
		ofSetColor(secondaryC);
		line.draw();

		ofPopMatrix();

		ofSetColor(mainC);
		line.draw();
	}
	//ofMap(stepSize, stepMin, stepMax, 3, 10);
}
*/


void Line::draw() {
	ofPushMatrix();
	ofTranslate(10, 6);
	int chance = ofRandom(0, 15);
	ofPolyline glitchedLine = glitch2();
	if (glitched && chance < 12) {
		ofSetColor(secondaryC);
		glitchedLine.draw();
		ofTranslate(stepSize/8, stepSize/8);
		ofSetColor(glitchC);
		glitchedLine.draw();
		ofPopMatrix();
		ofSetColor(mainC);
		glitchedLine.draw();
	}
	else {
		ofSetColor(secondaryC);
		line.draw();
		ofPopMatrix();
		ofSetColor(mainC);
		line.draw();
	}

}