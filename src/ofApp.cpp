#include "ofApp.h"

//NOTE: Implement some noise into this


//--------------------------------------------------------------
void ofApp::setup() {
	ofBackground(0, 0, 0);
	//ofSetFrameRate(1);
	ofSetFrameRate(9);
	Line newLine;
	newLine.setup();
	lines.push_back(newLine);
	Grid newGrid = Grid();
	newGrid.setup();
	grids.push_back(newGrid);
	//cout << "size of grids :" + std::to_string(grids.size()) << endl;
}

//--------------------------------------------------------------

bool shouldRemove(Grid &g) {
	if (!g.isAlive()) {
		return true;
	}
	else {
		return false;
	}
}
void ofApp::update() {


	for (int i = 0; i < grids.size(); i++) {
		grids[i].update();
		//if (!grids[i].isAlive) {
		//	grids.erase(grids.begin() + i);
		//	i++;
		//}

	}
	ofRemove(grids, shouldRemove);
	cout << "size of grids :" + std::to_string(grids.size()) << endl;
}

void ofApp::draw() {

	for (int i = 0; i < grids.size(); i++) {
		grids[i].draw();

	}

}


//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	if (key == 'c') {
		grids.clear();
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {
	mouse = { mouseX, mouseY, 0 };
	Grid newGrid = Grid();
	newGrid.setup();
	grids.push_back(newGrid);
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {
	//lines.clear();
	mouse = { mouseX, mouseY, 0 };
	Grid newGrid = Grid();
	newGrid.setup();
	grids.push_back(newGrid);

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
